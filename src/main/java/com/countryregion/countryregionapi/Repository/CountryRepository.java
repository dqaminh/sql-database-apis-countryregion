package com.countryregion.countryregionapi.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.countryregion.countryregionapi.Model.Country;

public interface CountryRepository extends JpaRepository<Country, Long> {
    Country findByCountryCode(String countryCode);
}
