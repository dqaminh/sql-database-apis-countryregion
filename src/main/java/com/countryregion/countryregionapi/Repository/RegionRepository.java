package com.countryregion.countryregionapi.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.countryregion.countryregionapi.Model.Region;

public interface RegionRepository extends JpaRepository<Region, Long> {
    
}
