package com.countryregion.countryregionapi.Controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.countryregion.countryregionapi.Model.Country;
import com.countryregion.countryregionapi.Model.Region;
import com.countryregion.countryregionapi.Service.CountryService;
import com.countryregion.countryregionapi.Service.RegionService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*",maxAge = -1)
public class CountryController {
    @Autowired
    private CountryService countryService;

    @Autowired
    private RegionService regionService;

    @GetMapping("/countries")
    public ResponseEntity<List<Country>> getAllCountries(){
        try {
            return new ResponseEntity<>(countryService.getAllCountries(),HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/regions")
    public ResponseEntity<Set<Region>> getRegionsByCountryCode(@RequestParam(value = "countryCode") String countryCode){
        try {
            Set<Region> regions = countryService.getRegionsByCountryCode(countryCode);
            if (regions != null) {
                return new ResponseEntity<>(regions, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/regions-all")
	public ResponseEntity<List<Region>> getRegionsAll() {
        try {
			return new ResponseEntity<>(regionService.getAllRegions(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
