package com.countryregion.countryregionapi.Service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.countryregion.countryregionapi.Model.Country;
import com.countryregion.countryregionapi.Model.Region;
import com.countryregion.countryregionapi.Repository.CountryRepository;

@Service
public class CountryService {
    @Autowired
    CountryRepository iCountryRepository;

    public ArrayList<Country> getAllCountries(){
        ArrayList<Country> lCountries = new ArrayList<>();

        iCountryRepository.findAll().forEach(lCountries::add);

        return lCountries;
    }

    public Set<Region> getRegionsByCountryCode(String countryCode){
        Country vCountry = iCountryRepository.findByCountryCode(countryCode);
        if (vCountry != null) {
            return vCountry.getRegions();
        } else {
            return null;
        }
    }
}
