package com.countryregion.countryregionapi.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.countryregion.countryregionapi.Model.Region;
import com.countryregion.countryregionapi.Repository.RegionRepository;

@Service
public class RegionService {
    @Autowired
    RegionRepository iRegionRepository;

    public ArrayList<Region> getAllRegions(){
        ArrayList<Region> lRegions = new ArrayList<Region>();

        iRegionRepository.findAll().forEach(lRegions::add);

        return lRegions;
    }
}
